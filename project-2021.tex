\documentclass[a4paper]{article}

\usepackage{hyperref}
\usepackage{amsmath}
% \usepackage{ebutf8}
\usepackage{csquotes}
\title{\vspace{-4em} Research project \\
  \large Towards a theory of programming languages}
\date{}
\author{Ambroise Lafont}
\newcommand{\AL}[1]{\textcolor{red}{AL: #1}}
\usepackage{etoolbox}

\usepackage{xspace}

\newcommand{\Cogent}{\textsc{Cogent}\xspace}
\newcommand{\cogent}{\Cogent}
\newcommand{\Dargent}{\textsc{Dargent}\xspace}
\newcommand{\dargent}{\Dargent}
\begin{document}

\maketitle

Formal verification of programs has significantly progressed since the last century. Let us
mention the verified compiler CompCert~\cite{compcert} and the Verified Software
Toolchain~\cite{vst} for the C language, as well as
Iris~\cite{iris}, a program logic implemented in Coq used to certify numerous
large programs, in multiple languages such as Rust~\cite{rustsafe} and OCaml~\cite{cosmo}.

% La preuve de programmes a considérablement progressé depuis les années 2000. Citons par
% exemple le compilateur certifié CompCert [20] et la Verified Software Toolchain [6] pour le langage
% C, ainsi qu’Iris [16], une logique de programmes implémentée en Coq ayant permis de certifier
% de nombreux programmes significatifs, dans plusieurs langages tels que Rust [15] et OCaml [22].


My research project aims at contributing to this domain by a fruitful mix of theoretical
investigations and concrete practical implementations. 
It focuses on two different research directions inherited from my PhD and my
postdoctoral position:
\begin{enumerate}
\item Constructing a mathematical
  theory of programming languages (Section~\ref{s:thprl}), i.e.,
  finding solid and stable mathematical notions of programming languages and
  compilation, so that various results, proof methods, and notions can be
  abstracted and generalised.
  An important effort will be devoted to finding adequate notions of
  signatures to specify and present programming languages and compilations,
  as well as implementing tools to offer specific computer-aided reasoning.
\item Certifying programs in a heterogeneous environment, focusing in particular on the concrete case of the \cogent
  programming language (Section~\ref{s:cogent}),
   a language for
  certified systems programming, which thus inherently faces
  interoperability issues.
  % However, theoretical approaches based on logic of programs do not yet account for systems 
  % involving multiple programming languages. 
\end{enumerate}

In Section~\ref{s:integration}, I explain how my research project
fits into the range of interests of three CNRS laboratories.

% on a vachement avance dans la verification de logicielle mais
% il y a encore des trucs a faire, ce programme se concentre sur
% deux directions de recherche heritees de ma these et mon postdoc:
% Toute bonne recherhce fait des allers-retours entre theorie et application
% mon projet de recherche a un volet theorique qui va nourrir et se nourrir
% d'un volet beaucoup plus applique
% 1. mathematisation de la semantique operationelle
% + notion de sig
% +programmation
% 2. interoperabilite
% le lecteur ne devrait pas etre surpris dans la section 2 et 3
% 
% 

% Then, presenting new programming languages 
% and compilations
% becomes easier, thanks to adequate notions of
% specification.

% In section~\ref{s:thprl}, I present my mid-term research program for building a formal and high-level notion of programming language and compilation. 
% To support this objective, this project aims at implementing computer-aided
% tools to reason about programming languages, as detailed in Section~\ref{s:tools}.

% The \cogent compiler is an example of safe compilation that this research
% project aims to account for.
% In Section~\ref{s:cogent}, I explain some extensions to the \Cogent framework
% I plan to implement and later abstract at a more general, theoretical level.

% choice in the quest for formal notions of programming language and compilation.
% In this respect, it is helpful to
% be involved in some practical compilation project to better understand the
% needs. In this respect, This language features subtyping, linearity, polymorphism, and its compilation to C is certified through multiple intermediate stages, involving a pure functional operational semantics as well as a stateful one. It therefore is a target of choice in the quest for formal notions of programming language and compilation.

% In a first section, I detail my mid-term research program for building a formal and high-level notion of programming language and compilation. In a second section, I present some software projects I would like to develop, either as an application of this theoretical activity or as tools to validate and guide some involved proofs, of specific kinds. In the third section, I finally  detail some future work about the safe compilation of the cogent programming language. 


\section{Theory of programming languages}
\label{s:thprl}


The development of a theory of programming languages can be split into two
research directions: first, devising a general notion of signature to specify
programming languages and compilations between them; second, generalising
well-known proof methods and algorithms. 

\subsection{Theory of signatures}


\paragraph{Linearity}
Linear types have been used~\cite{rust} to enforce memory safety properties at compile
time, for example by forbidding referring twice to the same memory location.
A general notion of programming language is expected to account for them.
Let us consider the paradigmatic case of linear
lambda-calculus. There, all variables must be used exactly once. As a
consequence, terms do not support arbitrary substitution, such as non injective
renamings. The mathematical notion of operads handles such
cases~\cite{tanakalinear},
although the
simply-typed setting has not been worked out yet. Programming languages that
feature linear variables typically support non-linear variables as well. This
happens for example in the quantum lambda-calculus, or in the \cogent programming
language, where linearity of variables depend on their types.
The operadic
setting needs to be investigated to accommodate such mixed syntax. Beginning
with the untyped case, I am looking for a monoidal product such that monoids
support parallel substitution respecting linearity. Recent
work in this direction by Hyland-Tasson~\cite{hylandtasson} seems promising,
although they are interested in slightly different linearity constraints.
The next step will be to
investigate how syntax of programming languages can be specified as monoids,
using the standard device of endofunctors with strengths~\cite{fiore:presheaf}.

\paragraph{Dependent types}


Whereas there is a standard notion of binding signature for simply-typed syntax, the case of dependently typed languages is still an active field of research. I plan to investigate this question by focusing particularly on
simultaneous substitution. In the simply-typed case, the monadic approach nicely
accounts for them, but it does not straightforwardly apply to the dependently
typed case, because of the nested dependent structure of contexts. Monadicity
fails
for simple examples, if one takes as basic notion of context a set of \emph{types} $Ty$ together
with, for each type $A\in Ty$, a set $Tm_A$ of \emph{terms of type $A$}.
Indeed, the category of contexts equipped with a type universe $U$ and an
explicit coercion from terms of type $U$ to types is not monadic.
However, the forgetful functor can be understood as the composition of two
monadic functors, by considering the intermediate category of contexts equipped
with an additional type $U$. Such decomposition suggests to consider nested
monads, or more precisely, compositions of monadic functors where the last one
targets the category of basic contexts. The idea is that each layer adds a new
construction to the dependently typed language. The link with other semantic
models of type theories needs to be worked out. It is already clear that the
domain category of this composition of monadic functors has a structure of
category with families~\cite{cwf}, under some technical conditions.
But the main challenge consists in designing a notion of specification for these
nested structures. In particular, stating an adequate recursion principle
is intricate because naive notions of signatures are not functorial, contrary
to the simply-typed case.

\subsection{Generalising results and algorithms}

Beyond providing a mathematical notion of programming languages,
this project aims at accounting for standard algorithms (such as compilers) and results.
% beyond
% A mathematical framework for programming languages should be able to account
% specific cases.
% Having a general notion of programming languages is not enough:
% la compilation congruence de la bisimilarite, techniquement il nous manque
% un premier point est le schema de recursion


\paragraph{Compilation}
The general notion of programming languages and compilations that this project
develops is meant to handle in the first place Plotkin's CPS
translations~\cite{PlotkinCPS}, compilation of lambda-calculus to the Zinc or Krivine machine, or
to combinatory logic. 
This requires further work on the available recursion principle.
Indeed, in my recent
work on a theory of syntax suited for De Bruijn encoding~\cite{debruijn}, the recursion
principle is not powerful enough to compile easily between languages that do not
share the same simple type system. Even the type erasure function that turns a
well-typed term into a raw, untyped one is not straightforward. The setting of
transition monads~\cite{fscd2020} can similarly be improved: the recursion principle can only be
used to compile a programming language into itself, but with different
evaluation rules. Such a case occurs for example in the \cogent programming
language, which is given two operational semantics, and a correspondence theorem
between those. However, compilations
typically involve different source and target syntax.
% 
% happen between
% this is not powerful enough and thus deserves special focus.
 
 I plan to exploit the recent work of
Arkor-Fiore~\cite{FioreArkorPolynomials} that enables recursion between syntax
with different type systems. In particular, I would like to adapt their work to
support arbitrary simultaneous substitution, rather than unary substitution,
as they did.
Regarding the case of operational semantics, I will take advantage on my previous work on reduction monads~\cite{popl20} to generalise the recursion principle.


Other algorithms that I plan to explore are type soundness results for common typing algorithms, such as bidirectional type systems, in particular for system F and some of its extensions (such as recursive types).


\paragraph{Congruence of bisimilarity}


One motivation for the quest of a formal notion of programming language is that
it then becomes possible to  state precise theorems about a well-defined class
of languages, instead of introducing proof methods  whose scopes are not always
clearly delimited. On this matter, I have worked on a general setting for Howe's
method to prove that applicative bisimilarity is a
congruence~\cite{lics2020,howesimpl}. It remains to be extended to cover more
programming languages, such as the higher-order pi-calculus, or lambda-calculus
with algebraic effects and handlers, or other useful notions of bisimilarity
such as the normal or environmental one.



\subsection{Local computer-aided reasoning}
% trop naif, trop ambitieux, pas assez structure
% la majorite des preuves sur papier on n'a aucun doute,
% mais il y a des resultats critiques a certains endroits
% faire des formalisaiton de ces parties la
% on est loin de faire un push-button qui va tout faire
% la mathematisation mise en oeuvre dans les premiers paragraphes
\label{ss:tools}


% Working out a formal theory of programming languages is one step towards
% automatic generation of correct compilers from adequate signatures. One
% long-term objective is indeed to develop a tool that lets the user specify
% programming languages and safe compilations between them. Beyond the practical
% benefit of easing compiler implementation, this software will come with a
% mechanisation ensuring correctness of the compilation. This formalisation
% could also be used to formally prove additional properties that the user cares
% about. Recent Agda mechanisations, such as Fiore-Szamozvancev's\footnote{\url{https://www.cl.cam.ac.uk/~ds709/agda-soas/}},
% already generate the syntax together with a well-behaved substitution function. On a
% syntactic level, they need to be extended to cover advanced features such as linearity, subtyping,
% polymorphism. I also plan to formalise operational semantics, with an adequate notion of specification following my work on transition monads~\cite{fscd2020}.

% un titre

% Mechanising a formal theory of programming languages may be too
% ambitious:
In a proof, difficulties are usually confined to specific particularly technical
fragments.
In this project, I propose to design computer tools to help proving 
correctness of such critical phases,
in the particular field of operational semantics, and more
generally programming language theory.
% Ensuring soundness of the definitions
% and proofs.

Such tools typically build upon successful proof assistants such as
Coq, Agda, or Isabelle/HOL, but not exclusively.

\paragraph{Graphical reasoning}  Categorical proofs often involve showing commutation of
diagrams of morphisms. Pen and paper are inconvenient for large diagrams, since
the exercise may require some layout reorganisation. Moreover, translating such
proofs in a text-based proof assistant such as Coq is tedious and repetitive.
Motivated by this state of affairs, I have started implementing a commutative
diagram
editor\footnote{\url{https://amblafont.github.io/graph-editor/index.html}}. The
current preliminary version merely allows the user to generate latex from drawn
nodes and arrows, but the goal is to interact with Coq, by generating proof
scripts from a commutative diagram or conversely, by building a commutative
diagram helped by proved Coq theorems. Alternative kinds of diagrams will be
investigated as well, such as string diagrams, e.g., when reasoning about monads. 

\paragraph{Solver for morphism equality}
To help in my recent research, I wrote a Coq tactic\footnote{\url{https://amblafont.github.io/stuff/eqsolver.v}}
that automatically proves equalities of morphisms involving monadic
structure.
% \footnote{TODO: mettre un lien}.
It relies on the definition of monads
as extension systems~\cite{extsys}: the tactic chooses an orientation of the monadic equations
to compute a normal form from a given composition of morphisms. Testing equality
of morphisms then amounts to comparing their normal forms.  I have extended and
used this tactic in the setting of a category with coproducts, equipped with a
distributive law of monads, to show commutation of large diagrams in some of my
recent research. Completeness of this tactic is not clear, and it
would be helpful to understand what is its exact scope.

\paragraph{Type-theoretic free monads}
% The other question I would like to explore focuses on formalising free monads in
% type theory.
Here, the motivation comes from an experiment\footnote{\url{https://amblafont.github.io/stuff/freemonad.agda}}
% \footnote{TODO: mettre un
% lien}
in Agda to prove 
commutation of a large diagram. I axiomatised an
endofunctor on the category of types, and then realised that its free monad
could be axiomatised as well, by introducing a type-theoretic (dependent) induction
principle. I was then able to prove the desired result more efficiently than
by the usual diagrammatic reasoning.
However, the current
method only applies to the category of sets. The project is to
generalise it to abstract categories with suitable structure, which
involves (1) delineating the structure in question, and (2) proving the
soundness, and hopefully completeness, of the method.
 This begs the question: what is the internal language
of categories equipped with a monad? Investigating this matter
will result in new means of proving and constructing morphisms in such settings,
beyond providing theoretical grounds for my experiment.
% , by replaying it in a specific type theory (to be worked
% out) corresponding
% to their internal language.


\section{Interoperability for \cogent}
% certifier la programmation  interoperatbilite 
\label{s:cogent}

The \cogent programming language is designed to ease formal verification of
systems code, thanks to its compilation to C which is certified through multiple
intermediate stages.
% , involving a pure functional operational semantics as well as a stateful one. 
% The compiler produces a formal proof (in
% Isabelle/HOL) that the generated C program refines the functional semantics of
% the original \Cogent program.


% The Cogent framework is an example of safe compilation that this research
% project aims to account for. The compiler produces a formal proof (in
% Isabelle/HOL) that the generated C program refines the functional semantics of
% the original Cogent program.
\Cogent programs are typically meant to operate with
external C code, which may be verified independently using the AutoCorres
library in Isabelle/HOL. Connecting such external formalisations with the
proofs generated by the \cogent compiler raises some challenges.
Beyond this interaction with C code, a device driver implemented in \cogent
needs to communicate with the device following a well-specified layout.

As far as formal verification is concerned, these interactions raise some
additional challenges, that we plan to solve for \cogent, before investigating
at a more theoretical level.
% Related to this environmental 

% \cogent
% First, these refinement theorems are cumbersome to use in practice. Simplifying
% and improving them is crucial, since \cogent is meant to gradually replace C code.

% One use case is the \cogent implementation and verification of device drivers.
% Interaction with external C code or peripheral device raises some challenges
% regarding formal verification.

% This raises some interoperability challenges
% In this case, \cogent is meant to interact % In this respect, my previous experiments have suggested various extensions.

\subsection{Verified bitfields}
Communicating with a peripheral device often requires careful data formatting,
which is prone to error, and whose formal specification is tricky.
In the context of my postdoctoral position, I have worked on an extension of the
language allowing the user to provide a layout that specifies how \cogent data-types
should be compiled. For example, a record consisting of eight booleans can be
compiled to a single byte in C. Getting or setting one of the boolean field then
compiles to a call to a generated getter or setter that fetches or updates the
right bit, according to the layout. It is highly desirable to systematically
provide a formal proof of this fact, which is crucial when communicating with
peripheral devices whose configuration registers follow a rigid format. As a
first step, I have already implemented  automatic tactics that prove that
getters and setters are only concerned with the bits mentioned in the field
layout. In fact, it is not obvious how to state the expected property in
general, because the field types (and values) can be arbitrarily complex and
nested. However, this task should be possible by exploiting the deep embedding of
\cogent in Isabelle/HOL, which provides an algebraic representation of \cogent types and values. A first application consists in extending my formal verification of a \cogent timer driver\footnote{\url{https://github.com/amblafont/timer-driver-cogent}} with the proof that the implementation respects the format of the device register.
Furthermore, register layouts sometimes feature non-standard sized integers,
e.g., identifiers on 21 bits. A first naive attempt consists in representing
them with standard 32-bit integers. This, however, breaks the refinement proof
between the compiled C code and the functional specification. Roughly speaking,
the reason is that one should never update a 21-bit field with an integer that
does not fit on 21 bits, but this precondition to the field setter is not
enforced with the naive attempt. A solution consists in extending \cogent with
new types, for arbitrary sized integers. The verification framework needs then
to accommodate these types and take the above mentioned precondition into
account.
% I have already experimented with this idea using cogent
% abstract types, a limited mechanism that enables restricted extensions of cogent with
% new types by the user.
A first application of this extension that I will consider consists in
implementing and verifying a driver for a CAN network peripheral device.
% On a broader scale, this is one step towards understanding how to formalise
% the semantics of drivers, as they typically involve such encoding.



\subsection{Memory model}


\cogent is not compatible with the common pattern used in
systems programming consisting in casting a structure pointer into another, to
simulate structure inheritance, even if it is done in external C code. The
reason is that the \cogent verification framework relies on an optional AutoCorres feature, called the heap abstraction. This feature simplifies the memory model by assuming that each memory location relates to only one C type. Pointer casting typically breaks this assumption. In order to handle theses cases, it is desirable to remove the dependency of the verification framework on this feature. More precisely, what needs to be adapted is the refinement proof between the C code and \cogent stateful semantics. This task could benefit from revisiting the way the heap abstraction is implemented, since the memory model of the stateful \cogent semantics is analogous to the heap abstracted one.

% Interoperability with existing compiled code is useful in practice, as it extends
% the source language with possible low-level instructions.
%  Working on the specific case of \cogent will help clarifying this
% phenomenon on a more theoretical level.

\section{Integration in CNRS laboratories}
\label{s:integration}
\subsection*{Team LoVe, at the LIPN Laboratory (Villetaneuse)}

My research fits in the line of research led by Damiano Mazza around types,
models, and programming language theory. For example, my quest for an adequate notion
of semantics for programming languages could lead to fruitful collaborations with
Damiano Mazza and Flavien Breuvart, exploiting their knowledge
on the monadic/comonadic formalisation of effects, involved in some variants of
computational lambda-calculus I am willing to account for.
Damiano Mazza could also help devising an operadic variant of transition monads in
order to account for linear types, as he has used the notion of 2-operads to
formalise programming languages~\cite{damiano:operads}.
My interest in
specifying the differential lambda-calculus in a proper manner would benefit
from interacting with members of the LoVe team such as Marie Kerjean and Giulio
Manzonetto, who are experts of this calculus.
Next, I hope that interacting with Micaela Mayero, an experienced Coq user, would
help mechanising the obtained definitions and results.


Finally, albeit not strictly related to this research proposal, Thomas
Seiller's work on the entropy of dynamical systems matches the interest
and expertise acquired during my Master's in theoretical physics.

\subsection*{Proofs and algorithms, at the LIX laboratory (Saclay)}
My research project would fit well especially in the Proofs and algorithms pole
of the LIX laboratory, which
focuses in particular on trustworthy and verifiable metatheory.
Dale Miller is an expert on syntax with variable binding, and it would be
interesting to compare the monadic approach with the higher-order abstract
one advocated by Kaustuv Chaudhuri.
Finally, Noam Zeilberger's familiarity with operads would help
with respect to my project around linearity. He has also worked on skew monoidal categories,
which arose frequently in my recent work on syntax with bindings, to account
for substitution.

% \subsection*{Team Cosynus, at the LIX laboratory (Palaiseau)}
% Research in the Cosynus team is about verifying programs through semantic analysis using
% categorical methods.
% As such, my research project would greatly benefit from
% integrating the team.
% I have already had successful interactions with Samuel Mimram when 
% generalising my formalisation of weak $ω$-groupoids (see report on previous work) 
% to weak $ω$-categories, following his ideas~\cite{finster-mimram}.
% Among the challenges that I have listed, I believe that addressing 
% dependently-typed languages would in particular result in fruitful
% collaborations. The team also has expertise on concurrent systems, that
% could help me address the π-calculus (Section~\ref{ss:pi-calcul}).

\subsection*{Team Plume, at the LIP laboratory (Lyon)}

The Plume team focuses in particular on verification and typed programming,
which makes it particularly attractive for my research project. I could
collaborate with Damien Pous on the formalisation aspect, as he is interested
in automation tactics. Olivier Laurent's expertise in linear logic is also
relevant for addressing linearity. Colin Riba could also help, with his
interest in logic and semantics. Moreover, my project of extending the metatheory of
transition monads by adapting known techniques such as Howe’s method would benefit
from Plume's expertise on coinduction.
Finally, I could collaborate with Yannick Zakowski (CASH research group),
on matters related to Coq certified compilation.
\bibliographystyle{acm}
\bibliography{biblio.bib}

\end{document}