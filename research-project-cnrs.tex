\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{ebutf8}
\usepackage{csquotes}
\title{\vspace{-4em} Research project \\
\large Towards a theory of programming languages}
\date{}
\author{Ambroise Lafont}
\newcommand{\AL}[1]{\textcolor{red}{AL: #1}}
\usepackage{etoolbox}

\begin{document}

\maketitle

 % For example, the well developped framework of mathematical operational
 % semantics introduced by Plotkin and Turi~\cite{plotkin:turi:bialgebraic} does not handle languages with binding variables, such
 % as λ-calculus.


A major breakthrough in the field of compilation correctness is the
pioneering verified C compiler CompCert developed by Leroy’s team. Mostly
implemented within the Coq proof assistant, its correctness is guaranteed by a
mechanised proof, avoiding the need for testing.
This provides a particular instance of verified compilation. However,
it does not currently fit in any proposed theoretical definition of programming languages and
their compilations: mathematical frameworks such as Plotkin and Turi's mathematical operational
semantics~\cite{plotkin:turi:bialgebraic} are still far from matching the practice.
% Yet, this question
% is crucial if want to devise tools generating correct compilations from
% elementary data.

This research project focuses on answering the following question:
What is a programming language?
The goal is to develop a mathematical understanding of
programming languages, compilations between them, and their metatheory in order to reduce the gap between
theory and practice.
The long-term objective is to provide a Coq-based tool that
would take a specification of a programming language as input and 
generate the corresponding mathematical object, together with
associated expected properties. Such a tool would require solid and stable mathematical definitions:
 this is the main focus of this research project.

 \paragraph{Relevance of my profile}
 A large part of this project revolves around the mathematical notion of transition monad 
 as programming language, that I have introduced with Tom and Andr\'e
 Hirschowitz~\cite{fscd2020}, based on the last chapter of my PhD thesis.
 Besides, I have worked on various formalisations.
  In particular, I formalised the syntactical part of the setting of transition monads (see
 the report on previous work).
 This experience will prove useful when it comes to developing the 
 Coq-based device mentioned above.
% In this respect, induction is an essential

% device, for example when it comes to recurse on the syntax, as it
% happens in compilations. Here, the categorical approach based
% on Initial Semantics is a useful tool providing a uniform account of recursion.

 \paragraph{Plan}
Transition monads suffer from some limitations, explained in the first section, that
suggest a few extensions.
In the second section, I list some more ambitious challenges, beyond the scope
of transition monads. The last two sections are devoted to listing
collaborations that would be beneficial for my research project.
Section~\ref{s:integration}, in particular, explains
% In the third section, I list collaborations that this
% project would benefit from.
% Finally, in the last section, I explain
% how 
%  how my researhc agenda fits nicely in three of CNRRS laboratoires and how being
%  located in each of these laboraties will benefitial in terms of complementing
%  my expertise and fits.
% will benefit my research agenda and them as well
%  I can integrate in three of the CNRS laboratories.
%  \AL{et les avantages que j'ai a y aller et leurs avantages
%  }
 how my research project
 fits into the range of interests of three CNRS laboratories.
 % and how being at one of these laboratories will magnify the impact of my research.

 % This project directly builds on my area of expertise and uses some of the
 % terminology introduced in the report on previous work that will be indicated.
 % \AL{Give it a name.}

\section{Extending the setting of transition monads}
Transition monads\footnote{See the report
  on previous work for a more introductory description motivating
  this notion.} provide a
mathematical notion of programming language, taking into account both syntax
and operational semantics. Transition monads are designed to systematically account for
substitution invariance, using the categorical notions of monad and
modules over them. They can be specified by algebraic
presentations, in the spirit of Initial Semantics: there is a notion of signature
for specifying them; to each signature is associated a category of models,
consisting of transition monads equipped with additional structure (e.g., some
operations with specified arities). The transition monad \emph{specified} by a
signature is (by definition) provided by the initial object in the category of
models.


% Contrary to the theory of mathematical operational semantics \`a la Plotkin-Turi~\cite{plotkin:turi:bialgebraic},
% transition monads straightforwardly account for higher-order languages such as
% λ-calculus. They also account for non-congruent reduction rules, as in weak
% λ-calculus, which is out of reach of alternative categorical related work
% on rewriting by
% Hamana~\cite{Hamana03},
% T.~Hirschowitz~\cite{HIRSCHOWITZ:2010:HAL-00540205:2}, and
% Ahrens~\cite{Ahrens16}.
Although transition monads capture a number of important examples such as
λ-calculus (big- or small- step semantics, call-by-value or call-by-name),
some variant of $π$-calculus and differential $λ$-calculus,
they suffer from some limitations.
For example, the definition of $π$-calculus as a labelled transition system is
out of reach, and the metatheory is not well developed.
% , morphisms do not account for all the standard
% notions of (correct) compilations.
These defects suggest some extensions that are described in the next
subsections.

\subsection{Addressing specific programming languages}
\subsubsection{Differential λ-calculus (short term)}

The differential λ-calculus can be organised into a transition monad without
too much pain. However, its specification remains challenging, as we now explain.
Recall that a transition monad specified by a signature comes with a recursion principle
induced by its initiality property in an appropriate category of models. In
previous work~\cite{popl20}, we provided a notion of signatures and models for particular cases
of transition monads, called reduction monads. There, the induced
recursion principle enables the simple construction of compilations targeting a
language with a different syntax. This remains to be adapted to the more general
setting of transition monads, for which the current notions of signatures and
models generate a recursion principle that only targets languages with the same
syntax (and possibly different semantics). Although this extension is not
expected to raise any serious difficulty, it involves changes to the notion of
signature that would make the differential λ-calculus harder to specify. The
difficulty with this calculus is that the operational semantic relies on two
intermediary operations constructed by recursion in the initial model:
it is not obvious how to replicate this construction in any model of the syntax.


\subsubsection{Computational λ-calculus (short/medium term)}
\label{ss:computational}
Another example of a
transition monad which is not obvious to specify is some variant of the
computational λ-calculus with big-step semantics~\cite{computational_lambda}, where
terms reduce to semantic values. The precise notion of semantic value depends on
a chosen semantic monad $T$ (for example, the state monad). In this example, the
difficulty lies in the sequential reduction rule for composing effectful terms,
which does not fit in the current notion of signature for transition monads
because of universal quantification on semantic values in the second premise:
\[
	\frac{M
    \Downarrow
    X
    \qquad \forall V,\ …
    % N[x := V]
    \Downarrow …}{
		% M\ \mathtt{to}\ x.N
    …
    \Downarrow
    …
    % \mathtt{bind}_T(V\mapsto Y_V)(X)
		}
	\]
  One option to explore is to think of this universally quantified premise
  as a possibly infinite set of premises, although this would open
  the door to infinitary reduction rules.

\subsubsection{π-calculus (short/medium term)}
\label{ss:pi-calcul}
The π-calculus is a calculus of concurrent processes.
When presented as an unlabelled reduction system, the $π$-calculus can be
organised as a transition monad.
However, practitioners tend to rely on a different presentation~\cite[Table~1.5 p38]{DBLP:books/daglib/0004377}
based on labelled
transition systems, which cannot.
The difficulty is, for
example, that the target of a reduction may refer to some channel that may
be either fresh or known, depending on the reduction rule: the current
definition of transition monads is too strict to allow this. This calls for 
a generalisation of this notion.

\subsection{Proving congruence of bisimilarity (short/medium term)}
\label{ss:howe}
Transition monads provide guarantees
about substitution (substitution laws, stability of transitions under
substitution, ...), but not much beyond. Replaying some well-known metatheorems
or methods is an obvious means of testing the relevance of these theoretical
definitions, and at the same time could reveal an unthought level of generality
for some already known results. For example, how can we adapt Howe’s method,
commonly used to show that bisimilarity is a congruence? I have recently
worked on some categorical generalisation of this method~\cite{lics2020} in a
simpler setting, but it remains to
explore how this can be adapted to the setting of transition monads.

\subsection{Plotkin's CPS translations (short/medium term)}

Morphisms of transitions monads provide a notion of compilation between
programming languages, with forward simulation as correctness property: if
a term reduces to another in the source language, then there is a
reduction between the compiled terms.  Other criteria may be considered (backward
simulations, bisimulations), yielding alternative notions of morphisms between
transition monads. For example, we can easily adapt our setting
to allow translating one reduction to any finite sequence of reductions between compiled terms.

It would be useful to have mathematical tools for generating
compilations with these alternative properties from elementary data. We propose
to start with replaying the most standard academic compilations in the setting
of transition monads: Plotkin’s CPS translations of
λ-calculus~\cite{PlotkinCPS}, translation of π-calculus to λ-calculus,
translation of λ-calculus to the Krivine or Zinc machine. The goal is to test and refine the definitions of
transition monads and their morphisms.
% An auxiliary Coq formalisation could also
% help refine the long-term objective of having an effective theory of programming
% languages and compilations in Coq amenable to caml extraction.


\section{Long-term goals: beyond transition monads}

\subsection{Linear λ-calculus}
\label{ss:linear-lambda}
In the context of my current postdoctoral position, I am working on Cogent~\cite{cogent}, a
functional programming language featuring linear types. The issue with linear
types is that the syntax does not define a monad on the category of sets as
usual: indeed arbitrary substitutions are not allowed, since they break
linearity. For example, the linear λ-calculus is a programming language with
substitution, which cannot be organised as a monad (and thus, not as a transition
monad), but could be organised as an operad. This suggests introducing an
operadic variant of transition monads.
% notion of \enquote{transition operad}.
% Going even further the simply typed case could lead to some connection with Leinster's
% operad of ω-categories, where the base category is a category
% of presheaves over the category of globes, which is not discrete, contrary to
% the simply typed case.

Finally, the possibility of mixing both linear and non-linear variables
(as in the Cogent programming language) should be investigated.

\subsection{Dependently-typed languages}
\label{ss:dep}
A type system for a programming language
provides a way to check at compile time that the program cannot go wrong.
Dependent type systems, such as the one underlying the Coq proof assistant are
actively studied, in particular for their strong connection with proof theory,
thanks to the fundamental Curry-Howard correspondence.
For the simpler untyped case, monads on the category of
sets provide a notion of syntax: the monad multiplication 
accounts for syntactic simultaneous substitution\footnote{See the report on
previous work for the example of $λ$-calculus.}. Although this idea can be
straightforwardly extended to simply-typed syntax, the case of dependent types
requires further investigation. The challenge here is to find a similar notion
of monads and modules, that could be used to define and specify such complex
syntaxes. This would provide an answer to the initiality conjecture
formulated by Voevodsky~\cite{initiality-conjecture}, a recent problem related to the semantics of dependent
type theories. This conjecture corresponds to the fact that the definition of a
type theory as an extrinsic syntax, i.e., as an untyped syntax with typing
judgements, is compatible with the intrinsic definition\footnote{See the report
  on previous work for a more detailed explanation on the differences between
  intrinsic and extrinsic definitions.}, as the initial object
in the suitable category of models. This conjecture has been recently solved by
de Boer-Brunerie-Lumsdaine-Mortberg~\cite{initiality-conjecture-solved}, based on the notion of models as categories
with families, for a specific dependent type theory. Our proposal would induce
another solution for an alternative notion of models and for a whole class of
dependent type theories.


\subsection{Register allocation and Initial Semantics}

Bridging the gap between mathematical methods and the complex algorithms used in
practice is a tremendous challenge. This long-term goal does not directly
relate to transition monads: it consists first
in studying the possibility of addressing low-level compilation phases, such as
register allocation, in the spirit of Initial Semantics. Some of these phases in
the CompCert compiler are not even written in Coq, but in Caml. This is the case
of graph colouring in register allocation, which is “difficult to prove
correct directly”: the output of the Caml program is validated on a per-program
basis by a Coq verifier. This hints that the categorical understanding of such
an algorithm is certainly even more challenging to achieve.


\subsection{A Coq front-end for verified compilation}
The overall long-term goal of the project is to devise a general Coq-based framework inspired by the
particular CompCert compiler. Hence, as far as possible, the previous theoretical
investigations will be formalised in Coq in
an effective manner amenable to a Caml extraction.
The user would simply provide a specification of
some programming language, as a signature for transition monads. Then, the
Coq tool would generate the transition monad matching the specification.
A similar pattern could be considered for constructing compilations between
programming languages.

Let us remind that the definitions and results regarding the syntactic part of
transition monads and their specifications are already formalised, as mentioned
in the report on previous work. 

% The
% theoretical aspect of this project would delimit a general framework in which
% the Compcert compiler fits. On the practical side, this could ease, for example,
% the adaptation of Compcert to other programming languages.

\section{Collaborations}
This research project directly builds upon my PhD thesis, supervised by
Nicolas Tabareau and Tom Hirschowitz.
Since transition monads were introduced with Tom and Andr\'e Hirschowitz,
I intend to maintain an active collaboration with them.

During my PhD, I also happened to work with Ambrus Kaposi (Eötvös Loránd
University, Hungary) in the domain of dependent type theories: this interaction
could help for my project about dependently-typed languages.

My current postdoctoral position (at UNSW, Australia) is the opportunity for me to work
on the Cogent programming language featuring linear types: my project about
linear calculi (Section~\ref{ss:linear-lambda}) would benefit from keeping in touch with my current team, including
Christine Rizkallah, Caroll Morgan, and Gernot Heiser.
Still in the context of my current position,
% I have also met the center for category theory from the nearby
% Macquarie university.
I have also met a research group from the nearby Macquarie university, including members
such as Richard Garner and Stephen Lack.
I intend to maintain interactions with these researchers, as their expertise
in category theory has already proved very helpful.

% and keeping in touch with researchers such as
% Richard Garner would help

% have already Beyond the new
% collaborations that I intend to create by integrating some CNRS laboratories (as
% explained in the next section), I also intend to work with researchers that
% could specifically help for the challenges described in the previous sections
% such as Christine Rizkallah (as an expert on Cogent), for the extension to
% linearity, and Thomas Ehrhard (as an expert on differential lambda-calculus).


\section{Integration in CNRS laboratories}
\label{s:integration}
\ifdef{\sectionmath}{
\subsection*{Team Algebra, Topology and Geometry, at LJAD (Nice)}
My research project has been inspired by and will certainly involve collaborating with Andr\'e
Hirschowitz, one of the emeritus professor of this team.
Interacting with other members such as Carlos Simpson or Clemens Berger
could help in my project about dependently-typed languages (Section~\ref{ss:dep}), since they are
experts on higher categories which strongly relate to the semantics of such
languages. Clemens Berger's publication record demonstrates
a knowledge of the theory of operads that I could benefit from for my project of
extending to linear calculi (Section~\ref{ss:linear-lambda}).
I also expect fruitful interactions with Mai Gehrke who works on logic and
universal algebra, which closely relates to my concerns about
specifying mathematical objects (in particular, transition monads).

Finally, Carlos Simpson's interest in the formalisation of mathematics would
also match the verification aspect of this research project.
Besides, I have some side project about ω-categories, not strictly related to
this research proposal, that would highly benefit from Carlos Simpson's
expertise in this domain.
\subsection*{Team LDP, at Institut Math\'ematiques de Marseille}  
My quest for an adequate notion of semantics for programming languages fits well
within the scope of the team LDP which
includes homotopical theory of calculus and formal verification.
My project about dependently-typed languages (Section~\ref{ss:dep}), for example, could meet
Yves Lafont's interests.
My goal of specifying the differential λ-calculus in a proper manner would benefit
from interaction with Laurent Regnier or Lionel Vaux, who are experts
of this calculus. Linear logic is also within the scope of the team:
by the Curry-Howard correspondence, it strongly
relates to linear calculi, mentioned in Section~\ref{ss:linear-lambda}.
% lead to fruitful collaborations with

\subsection*{Team Algebra, geometry, logic, at Institut Camille Jourdan (Villeurbanne)}
Joining this team would provide me support on the category-theoretic side.
For example, my project about linear λ-calculus (Section~\ref{ss:linear-lambda}) involving the notion of operad
would benefit from interactions in this team.
I plan specifically to interact with Philippe Malbos who could help
me in my project about linear calculi or dependently-typed languages (Section~\ref{ss:dep}): his
experience with operads and homotopy could open unexpected perspectives.
His interest about rewriting could
relate to my search for a formal notion of operational semantics.
% Finally, this team has also some focus on theoretical physics which I am
% still interested in since my master in this domain.
Finally, albeit not strictly related to this research proposal, the research
theme of this team about mathematical physics matches my interests
and expertise acquired during my Master's in theoretical physics.
}{
\subsection*{Team LoVe, at the LIPN Laboratory (Villetaneuse)}

My research fits in the line of research led by Damiano Mazza around types,
models, and programming language theory. For example, my quest for an adequate notion
of semantics for programming languages could lead to fruitful collaborations with
Damiano Mazza and Flavien Breuvart, exploiting their knowledge
on the monadic/comonadic formalisation of effects, involved in the variant of
computational λ-calculus I am willing to account for (Section~\ref{ss:computational}).
Damiano Mazza could also help devising an operadic variant of transition monad in
order to account for linear types, as he has used the notion of 2-operads to
formalise programming languages~\cite{damiano:operads}.
My interest in
specifying the differential λ-calculus in a proper manner would benefit
from interacting with members of the LoVe team such as Marie Kerjean and Giulio
Manzonetto, who are experts of this calculus.
Next, I hope that interacting with Micaela Mayero, an experienced Coq user, would
help mechanising the obtained definitions and results.


Finally, albeit not strictly related to this research proposal, Thomas
Seiller's work on the entropy of dynamical systems matches the interest
and expertise acquired during my Master's in theoretical physics.

\subsection*{Team Partout, at the LIX laboratory (Saclay)}
My research project would fit well in the PARTOUT team, which
is interested in the question of trustworthy and verifiable meta-theory.
Dale Miller is an expert on syntax with variable binding, and it would be
interesting to compare the monadic approach with the higher-order abstract
one advoacted by Kaustuv Chaudhuri.
Finally, Noam Zeilberger's familiarity with operads would certainly be helpful
with respect to linearity. He has also worked on skew monoidal categories,
which arose frequently in my recent work on syntax with bindings, to account
for substitution.

% \subsection*{Team Cosynus, at the LIX laboratory (Palaiseau)}
% Research in the Cosynus team is about verifying programs through semantic analysis using
% categorical methods.
% As such, my research project would greatly benefit from
% integrating the team.
% I have already had successful interactions with Samuel Mimram when 
% generalising my formalisation of weak $ω$-groupoids (see report on previous work) 
% to weak $ω$-categories, following his ideas~\cite{finster-mimram}.
% Among the challenges that I have listed, I believe that addressing 
% dependently-typed languages would in particular result in fruitful
% collaborations. The team also has expertise on concurrent systems, that
% could help me address the π-calculus (Section~\ref{ss:pi-calcul}).

\subsection*{Team Plume, at the LIP laboratory (Lyon)}

The Plume team focuses in particular on verification and typed programming,
which makes it particularly attractive for my research project. Damien Pous’
interest in Coq and automatic proof would help refine the goals of my project
regarding formalisation. Moreover, my project of extending the metatheory of
transition monads by adapting known techniques such as Howe’s method (Section~\ref{ss:howe}) would benefit
from Plume's expertise on coinduction.

}

% \subsection*{Team PPS, at the IRIF laboratory (Paris)}

% I believe that my project is in line with the research themes of the IRIF
% laboratory, especially regarding the PPS team. My work on transition monads
% already made me familiar with some of the work of IRIF researchers. For example,
% we tackle some variant of λ-calculus with explicit substitution introduced by
% Delia Kesner. Also, as I have mentioned above, the differential lambda calculus,
% invented by Thomas Ehrhard and developed with Michele Pagani and Christine
% Tasson, is a motivating case for me. More generally, the project of extending
% the definition and specification of transition monads would benefit from
% interacting with researchers at IRIF who are devising programming languages in
% the context of their own research. IRIF has also an emphasis on dependent type
% theory, with the development of the Coq proof assistant. I have some ongoing
% project about inductive-inductive datatypes could be of some interest for
% researchers involved on the Coq (pratical or theoretical) development, such as
% Hugo Herbelin, leading to fruitful interactions. Finally, I also have some
% ongoing work on weak ω-categories, or more generally, in higher category theory,
% could involve collaborations with Paul-André Melliès or Pierre-Louis Curien.

\bibliographystyle{acm}
\bibliography{biblio.bib}

\newpage
\section{Theory of programming languages}
\subsection{Linearity}
In linear lambda-calculus, variables are linear: they must be used exactly once. The standard monadic approach does not apply, since terms do not support arbitrary substitution (e.g., non injective renamings of variables). Operads are more relevant to handle such cases. Programming languages that feature linear variables typically support non-linear variables as well (e.g., quantum linear lambda calculus). The operadic setting needs to be investigated further to accommodate such mixed syntax. More precisely, I plan to explore the specification of non-linear operadic operations, using an adequate alternative monoidal product. Tackling this issue is one step towards treating the cogent programming language.
Generalising results and algorithms
One motivation for the quest of a formal notion of programming language is that it then becomes possible to  state precise theorems about a well-defined class of languages, instead of introducing proof methods  whose scopes are not always clearly delimited. On this matter, I have worked on a general setting for Howe's method to prove that bisimilarity is a congruence. It remains to be extended to cover more programming languages, such as HOpi calculus. Other examples that I plan to explore is common typing algorithms, such as bidirectional type systems, in particular for system F and some of its extensions (such as recursive types).
\subsection{Recursion}
The scope of the recursion principle of a programming language is a feature that deserves some focus. For example, in the setting of de Bruijn monads, it is not obvious how to build compilation by recursion between languages that do not share the same simple type system. Even the type erasure function that turns a well-typed term into a raw untyped one is not straightforward. Another example is the setting of transition monads: the recursion principle can only be used to compile a programming language into itself, but with a different semantics. Such a case occurs for example in the cogent programming language, where two operational semantics are given: a functional semantic and a stateful one, with a correspondence between those. 

\subsection{Coq device}
One long term goal is to develop a Coq library that let the user specify programming languages and compilations between them from elementary data. This requires mechanising the construction of syntax from a signature. One notion of signature is that of an accessible strong endofunctor; the syntax generated in this general case remains to be mechanised. What is also missing is a formalisation of the operational semantics, following the theory of transition monads.
Imperative languages
In previous work, I have mainly focused on idealised functional languages. The case of imperative language remains to be explored. As a first example, the computational lambda calculus can be turned into a transition monad, but the operational semantics cannot be specified by the current notion of signature for transition monads. The cogent programming language also features a stateful operational semantics. Accounting for its compilation to C would require to handle some fraction of the C programming language.
Dependent types
The quest for a formal notion of dependently typed languages has led to multiple recent publications. The monadic approach that covers simply-typed languages with parallel substitution does not obviously apply because of the nested dependent structure of contexts. Instead of monads, categories with families (CwFs) are are popular models of dependent type theories. I hope to tackle this topic by noting that any cocomplete category C equipped with a right adjoint to the category of families can be turned into a CwF. My goal is to devise a general notion of signatures for dependent types theories.

\section{Tools for reasoning}

\subsection{Diagram editor}


Categorical reasoning often involves showing commutation of diagrams of morphisms. Pen and paper are inconvenient for large diagrams, since the process may require some layout reorganisation. Moreover, translating these proofs in a text-based proof assistant such as Coq is tedious and uninteresting. This was a motivation to implement a commutative diagram editor. The current version is preliminary in the sense that it merely allows the user to draw nodes and arrows and generate latex. In the future, I plan to extend the tool to generate Coq scripts. It remains to explore how these ideas could be generalised to other kinds of diagrams, for example, addressing equality coherence in dependent type theories.

\subsection{Proof assistant for category theory}


Proof assistants based on dependent types such as Coq or agda are convenient to formalise category theory. However, type dependency sometimes introduces a lot of complexit coming from the equality type explicit coercions induced by equality between types. This has been exemplified by some recent formalisation of schemes in the Lean theorem prover, where the authors "ran into a huge problem because [their] blueprint, the Stacks Project, assumed that R[1/f ][1/g] = R[1/f g], and this turns out to be unprovable for Lean’s version of equality".  However, schemes were also formalised in the Isabelle/HOL theorem prover without dependent types, and this issue didn't occur at all. This example suggests to investigate  formalising category theory in Isabelle/HOL theorems. Alternatively, it may be worth thinking about whether a proof assistant specifically designed for categorical reasoning could reduce formalisation overhead compared to generic proof assistants aiming at embedding the full mathematics.

\section{Cogent}


\subsection{Extend AutoCorres (long term)}


Cogent relies on AutoCorres, an Isabelle/HOL library that parses C code and embeds it in Isabelle as a monadic function, accounting for non-determinism, failure, and states. It was notably used to verify Sel4, the first fully verified operating system microkernel. AutoCorres accounts for a subset of the C programming language. In particular, it suffers from some strong limitations, that prevent some further extensions of Cogent. One of the most stringent one is the unability to take the address of a local variable (on stack). Extending AutoCorres to account for such features would greatly simplify and less convoluted systems code.

\subsection{Pointer casting}


The C program generated by the cogent compiler is typically meant to operate with external C code. Such C code may also be verified on its own, using the AutoCorres library. However, they sometimes involve pointer casting, a feature which is often incompatible with AutoCorres' heap abstraction,  that simplifies the memory model, by assuming that each memory location relates to only one C type. Unfortunately, cogent currently relies on this additionnal abstraction layer. The question is not yet settled whether it is actually possible to mix parts relying on the heap abstraction and other parts that do not in a safe way. Another possiblity to investigate is to remove the dependency of cogent.

\subsection{Specification of getters/setters}


The Dargent extension allows the programmer to specify precisely how cogent records are to be laid out in memory. A cogent record is then compiled to an array of bytes, rather than a C structure, and accessing a field translates to a call to a custom generated getter that fetches the data in the array according to the layout. It is desirable to prove automatically that getters indeed respect the given layout, and similarly for setters: the current verification framework only checks that they are concerned with the bits taken by the field layout.

\subsection{Coq verification}


Cogent's current verification process consists in compiling both to C and Isabelle and generating a correspondence proof between them, in multiple stages. One major drawback is that Isabelle takes a long time to accept the full proof. To mitigate this issue, we have started investigating another compilation path through Coq, strongly inspired by the vellvm library: the idea is to write the cogent compiler in Coq and proves it correct using interaction trees. In this way, the compiler does not need to generate any certificate, and thus there is no need for the proof assistant to check any proof a posteriori for each input cogent program.

\end{document}
