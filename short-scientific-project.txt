My research focuses on developing a mathematical setting to define and specify programming languages and safe
compilations between them. My research project proposes some challenges about transition monads, a categorical notion
of programming language: developing their metatheory, extending them to tackle languages with linear types, subtyping, or currently out-of-reach languages such as the computational lambda-calculus, or the pi-calculus as a labelled transition system.
I also give various possible research questions regarding dependent type theories that I am interested in, such as compiling inductive-inductive datatypes, finding a generalisation of transition monads to tackle dependently typed languages.
